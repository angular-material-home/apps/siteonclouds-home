/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('siteoncloudsHome')//

/**
 * @ngdoc controller
 * @name siteoncloudsHome.controller:MainCtrl
 * @memberof siteoncloudsHome
 * @description # MainCtrl Controller of the siteoncloudsHome
 * 
 * کنترلر اصلی سیستم هست که کل نرم افزار را مدیریت می‌کند. این کنترل در
 * صفحه اصلی به کار گرفته شده و سایر بخش‌های سیستم به عنوان بخش‌هایی از
 * این کنترل در نظر گرفته می‌شود.
 * 
 * این کنترل در فایل اصلی سیستم استفاده می‌شه و تنها یکبار زمان اجرای
 * سیستم فراخوانی خواهد شد. استفاده از این کنترل در جاهای دیگه سیستم
 * منجر به بهم ریختگی سیستم خواهد شد.
 * 
 * @deprecate no need any more
 */
.controller('MainCtrl', function () {

});
