'use strict';

/**
 * @ngdoc overview
 * @name siteoncloudsHome
 * @description # siteoncloudsHome
 * 
 * Main module of the application.
 */
angular.module('siteoncloudsHome', [
	'seen-shop',//
	'mblowfish-language',

	'ngMaterialHomeUser', //
	'ngMaterialHomeSpa', //
	'ngMaterialHomeTheme', //
	'ngMaterialHomeBank', //
	'ngMaterialHomeShop' //
])//
.config(function($localStorageProvider) {
	$localStorageProvider.setKeyPrefix('siteoncloudsHome.');
})
.run(function ($app, $toolbar) {
	// start the application
	$toolbar.setDefaultToolbars(['amh.owner-toolbar']);
	$app.start('app-siteonclouds-home');
});