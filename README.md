# Angular Material Home Shop (SiteOnClouds Home)

[![pipeline status](https://gitlab.com/angular-material-home/apps/siteonclouds-home/badges/master/pipeline.svg)](https://gitlab.com/angular-material-home/apps/siteonclouds-home/commits/master)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/33c1a8e8f3534e2e9f97f55b3fe0696e)](https://www.codacy.com/app/amh-apps/siteonclouds-home?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=angular-material-home/apps/siteonclouds-home&amp;utm_campaign=Badge_Grade)

It is an Angular Material SPA for selling products and services. It supports main functionlity of Shop module of Seen servers. This is the default SPA for tenants on siteonclouds.com.